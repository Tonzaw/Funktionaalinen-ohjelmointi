(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args])

;v5 01 teht 4
(defn square [num] (* num num))

;v5 01 teht 5
(defn karkausvuosi? [vuosi]
  (cond (zero? (mod vuosi 400)) true
        (zero? (mod vuosi 100)) false
        (zero? (mod vuosi 4)) true
        :default false))
      
;v6 01 teht 1
(def sum #(reduce + %))
(def avg #(/ (+ %1 %2) 2))
(def average #(/ (sum %) (count %)))

(def year2015 [-3 -4 0 3 5 8 12 16 20 12 10 4])
(def year2016 [-1 -2 0 4 7 5 10 22 15 8 8 3])

(println 
        (average  (filter #(> (min %) 0) 
                          (map avg year2015 year2016))))


;v6 01 teht 2
(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
 
 
(println (sum (map  #(- (get % :neste) (get % :vesi))
                    (filter #(= (:kk %) 4) food-journal))))
                
;v6 01 teht 3
(def kk (map  #(dissoc % :paiva :neste :vesi)
              (filter #(= (:kk %) 4) food-journal)))
                  
(def paiva (map #(dissoc % :kk :neste :vesi)
                (filter #(= (:kk %) 4) food-journal)))
                  
(def muuneste (map  #(- (get % :neste) (get % :vesi))
                    (filter #(= (:kk %) 4) food-journal)))

(defn unify-data [kk paiva muuneste]
  { :kk (get kk :kk)
    :paiva (get paiva :paiva)
    :muuneste muuneste
  }
)

(println (map unify-data kk paiva muuneste))

;v5 02 teht 1 ja 2
(loop [iteration 0]
      (if (= iteration 0)
          (println "Anna numero joka on suurempi kuin 0")
          (println "Numerosi oli pienempi tai yhtäsuuri kuin 0. Anna suurempi kuin 0"))
      (let [num (read-line)]
           (if (<= (Integer. num) 0)
               (recur (inc iteration))
               (if (= (mod (Integer. num) 2) 0)
                   (println "Numero on parillinen")
                   (println "Numero on pariton")))))
               
;v5 02 teht 3
((fn [raja]
    (loop [iteration 1]
          (if (<= iteration raja)
              (if (= (mod iteration 3) 0)
                  (do (println iteration)
                      (recur (inc iteration)))
                  (recur (inc iteration)))))) 12)
              
 
;v5 02 teht 4
(loop [iteration (set #{})]
      (if (< (count iteration) 7)
          (do (conj iteration (+ (rand-int 38) 1))
              (recur (iteration)))
          (sorted-set iteration)))
      
      
;v5 02 teht 5
(defn syt [p q]
     (if (= q 0)
         p
         (recur q (mod p q))))
(syt 232 56)