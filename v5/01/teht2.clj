(2*5)+4 => (+ 4 (* 2 5))

(1+2+3+4+5) => (+ 1 2 3 4 5)

((fn [name] (str "Tervetuloa Tylypahkaan " name)) "Toni")

(get-in {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}} [:name :middle])
