@Test
    public void testmoveAllPointsRightBy() throws Exception {
        List<Point> points = Arrays.asList(new Point(5, 5), new Point(10, 5));
        List<Point> expectedPoints = Arrays.asList(new Point(15, 5), new Point(20, 5));
        List<Point> newPoints = Point.moveAllPointsRightBy(points, 10);
        assertEquals(expectedPoints, newPoints);
    }
    
public class Point {
    
        private static int x;
        private static int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static int getX() {
            return x;
        }
        
        public static int getY() {
            return y;
        }

        public static Point moveRightBy(int x) {
            return new Point(getX() + x, getY());
        }

        public static List<Point> moveAllPointsRightBy(
                List<Point> points, int x) {
            return points.stream()
                    .map(p -> moveRightBy(x))
                    .collect(toList());
        }
}