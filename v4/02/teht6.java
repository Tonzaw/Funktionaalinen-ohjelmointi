
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class v4teht6 {

    public static void main(String[] args) {

        UnaryOperator<Dokumentti> ylimaaraisetValit = (Dokumentti doc) -> {
            System.out.println("Ylimääräiset välit poistetaan.");
            doc = new Dokumentti(doc.getTeksti().replaceAll("\\s+", " "));
            return doc;
        };

        UnaryOperator<Dokumentti> skandit = (Dokumentti doc) -> {
                System.out.println("Vaihdetaan scandit");
                doc = new Dokumentti(doc.getTeksti()
                        .replace("ä", "a")
                        .replace("Ä", "A")
                        .replace("å", "a")
                        .replace("Å", "A")
                        .replace("ö", "o")
                        .replace("Ö", "O"));
            return doc;
        };

        UnaryOperator<Dokumentti> sturctSanat = (Dokumentti doc) -> {
                System.out.println("Vaihdetaan sturct -> struct");
                doc = new Dokumentti(doc.getTeksti().replaceAll("\\bsturct\\b", "struct"));
            return doc;
        };


        Function<Dokumentti, Dokumentti> ketju = ylimaaraisetValit.andThen(skandit).andThen(sturctSanat);

        Dokumentti doc = new Dokumentti("Apina  hyppäsi puusta kåäpiön niskaan.   Joku kirjotti sturct väärin.");
        System.out.println("Ennen: " + doc.getTeksti());

        doc = ketju.apply(doc);

        System.out.println("Jälkeen: " + doc.getTeksti());

    }

}



public class Dokumentti {

    private String teksti;

    public Dokumentti(String teksti) {
        this.teksti = teksti;
    }

    public String getTeksti() {
        return teksti;
    }

    public void setTeksti(String teksti) {
        this.teksti = teksti;
    }

}
