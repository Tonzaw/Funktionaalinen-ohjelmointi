package v4teht5;

public class TuoteFactoryLaktoositon extends Meijeri{

    public TuoteFactoryLaktoositon() {
        map.put("MAITO", Maito::new);
        map.put("JUUSTO", Juusto::new);
        map.put("JUGURTTI", Jugurtti::new);
    }

}
