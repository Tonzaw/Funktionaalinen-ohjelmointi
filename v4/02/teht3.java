import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

import static java.util.stream.Collectors.toList;

public class v4teht3 {

    public static void main(String[] args){

        Omena o1 = new Omena("vihreä", 100);
        Omena o2 = new Omena("vihreä", 110);
        Omena o3 = new Omena("punainen", 101);
        Omena o4 = new Omena("punainen", 105);

        List<Omena> omenat = new ArrayList<>();

        omenat.add(o1);
        omenat.add(o2);
        omenat.add(o3);
        omenat.add(o4);

        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);

        long start = System.nanoTime();

        List<Double> l = DoubleStream.generate(Math::random)
                .limit(50000)
                .boxed()
                .collect(new OmaListaKollektori<>());  // ikioma kollektori
        //.collect(toList()); // Javan oma

        long duration = System.nanoTime() - start;

        System.out.println(l.size() + " " + duration / 1000000);

        // duration noin 20 Javan kollektorilla
        // duration noin 500 omalla kollektorilla
        // en huomaa eroa ajassa parallelina

    }

}
