public class teht2 {
    
    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (pituus) -> kPiste == pituus ? 60 : ((pituus - kPiste) * lisapisteet) + 60;
    };
        
    public static void main(String[] args) {

       DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 2.0);
       
       System.out.println(normaaliLahti.applyAsDouble(80.0)); 
          
    }
    
}