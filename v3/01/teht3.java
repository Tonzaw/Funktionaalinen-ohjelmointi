import java.util.Random;

public class teht3 {
    public static void main(String ...args){

        int sixes = new Random()
                .ints(20, 1, 7)
                .filter(num -> num == 6)
                .reduce(0, (a,b) -> a + 1 );
        System.out.println(sixes);

    }
}
