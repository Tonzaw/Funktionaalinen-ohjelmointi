import java.util.function.Function;

public class teht1 {

    public static void main(String...args){
        Function<Double,Double> toCelsius = fahrenheit -> (double) ( (fahrenheit - 32) * 5/9);
        double celsius = toCelsius.apply(100.0);
        System.out.println(celsius);

        Function<Integer,Double> getArea = radius -> Math.PI * radius * radius;
        double area = getArea.apply(100);
        System.out.println(area);
    }

}
