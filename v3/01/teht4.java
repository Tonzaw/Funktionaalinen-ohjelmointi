import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class teht4 {
    public static void main(String ...args){

        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(3, 4);

        List<int[]> pairs = list1.stream()
                .flatMap(i -> list2.stream().map(j -> new int[] { i, j }))
                .collect(toList());

        pairs.forEach(i -> System.out.println("{" + i[0]+ "," + i[1]+ "}"));
    }
}