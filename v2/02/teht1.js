
function maki(kPiste, lisapiste) {
    return function pisteet(pituus) {
        return kPiste == pisteet ? 60 : ((pituus - kPiste) * lisapiste) + 60;
    };
}

let hyppaaNormMaki = maki(75, 2);
let hyppaaIsoMaki = maki(100, 1.8);

console.log(hyppaaNormMaki(80));
console.log(hyppaaIsoMaki(125));
