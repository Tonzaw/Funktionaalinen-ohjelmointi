const Immutable = require('immutable');

var firstTenElements = Immutable.Repeat()
                                .map((x = 1) => x + 1)
                                .take( 10 )
                                .toJSON();

console.log(firstTenElements);