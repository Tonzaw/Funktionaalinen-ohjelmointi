'use strict';

const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    
    class Auto{
        constructor(){
            suojatut.set(this, {tankki: 0, matkamittari: 0});
        }
        
     
        aja() {
            let mittari = this.getMatkamittari();
            mittari += 1;
            let polttoaine = this.getTankki();
            polttoaine -= 1;
            suojatut.set(this, {tankki: polttoaine, matkamittari: mittari});
        }
        
        tankkaa() {
            suojatut.set(this, {tankki: this.getTankki() + 40, matkamittari: this.getMatkamittari()});
        }
        
        getMatkamittari() {return suojatut.get(this).matkamittari;}
        
        getTankki() {return suojatut.get(this).tankki;}
        
    }
    
    return Auto;
})();


const auto1 = new Auto();
console.log("Mittari: " + auto1.getMatkamittari());
console.log("Tankki " + auto1.getTankki());
auto1.tankkaa();
auto1.aja();
console.log("Mittari: " + auto1.getMatkamittari());
console.log("Tankki " + auto1.getTankki());
auto1.aja();
console.log("Mittari: " + auto1.getMatkamittari());
console.log("Tankki " + auto1.getTankki());