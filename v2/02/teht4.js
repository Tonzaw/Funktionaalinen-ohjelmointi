const Immutable = require('immutable');

let tulos = Immutable.Range(1, Infinity)
    .map(function(n){console.log(`map ${n}`); return n + 2})
    .filter(n => n % 13 === 0)
    .take(2)
    .reduce((a, n) => a += n, 0);
    
console.log(tulos);