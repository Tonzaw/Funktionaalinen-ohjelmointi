/*
function toCelsius(fahrenheit) {
    return (5/9) * (fahrenheit-32);
}

function toArea(radius) {  
    return Math.PI * radius * radius;  
}
*/

var celsius = (fahrenheit) => (5/9) * (fahrenheit-32);
var area = (radius) => Math.PI * radius * radius; 

console.log(celsius(100));
console.log(area(100));