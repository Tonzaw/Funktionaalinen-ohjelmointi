let fs = require("fs");

var text = fs.readFileSync('kalevala.txt','utf8');

text = text.replace(/[^a-zA-Z0-9 ]/g, '');
text = text.replace(/\s{2,}/g, ' ').toLowerCase();

let words = text.split(' ');

let wordcnt = words.reduce(function(map, word) {
                            map[word] = (map[word]||0)+1;
                            return map;
                            }, Object.create(null)
                        );

console.log(wordcnt);