let year2015 = [-3, -1, 0, 3, 7, 12, 23, 15, 17, 6, 2, -5];
let year2016 = [-2, 2, 5, 5, 9, 10, 22, 17, 13, 8, 1, -6];

let mean = (itemValue, index) => {
    return (itemValue + year2016[index]) / 2;
};

let positives = (item) => {
    if (item >= 0) {
        return true;
    }
    return false;
};

let meanPositives = (accumulator, currentValue, currentIndex, array) => {
    if (currentIndex == array.length - 1) {
        return (accumulator + currentValue) / array.length;
    }
    return accumulator + currentValue;
};


console.log(year2015.map(mean)
                    .filter(positives)
                    .reduce(meanPositives));