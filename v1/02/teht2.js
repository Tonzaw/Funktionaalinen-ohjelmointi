'use strict'

const f = function () {
    return function (x, y) {
        if (x == y) {
            return 0;
        } else if (x > y) {
            return 1;
        } else {
            return -1;
        }
	};
}();

var x = function(a, b, c) {
    let sum = 0;
    for (let i = 0; i < b.length; i++) {
        if (a(b[i], c[i]) == 1) {
            sum++;
        }
    }
    return sum;
};

let yearA = [12, 23, 15, 17, 6, 2, -5];
let yearB = [10, 22, 17, 13, 8, 1, -6];

console.log( x(f, yearA, yearB) );