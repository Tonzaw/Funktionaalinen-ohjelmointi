/*
foo() ajaa funktion, joka antaa f ja g muuttujille funktiot ja asettaa x:n arvoksi 1.
foo funktion sisällä kutsutaan f funktiota, joka kasvattaa x:n arvoa yhdellä ja palauttaa sen.
g() kutsuu g muuttujan arvona olevaa funktiota, joka vähentää x:n arvoa yhdellä ja palauttaa sen.
f() kutsuu f muuttujan arvona olevaa funktiota, joka kasvattaa x:n arvoa yhdellä ja palauttaa sen.

Ideana on, että on kaksi muuttujaa, jotka pääsevät käsiksi samaan muuttujaan sulkeuman avulla.
*/