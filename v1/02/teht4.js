function potenssi(x, y) {
    return potenssiHelper(x, y, 1);
}

function potenssiHelper(x, y, sum) {
    if (y == 0) {
        return sum;
    }
    return potenssiHelper(x, --y, x * sum);
}

console.log(potenssi(8, 4));