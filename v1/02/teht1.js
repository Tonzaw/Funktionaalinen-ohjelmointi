'use strict'

const f = function () {
    return function (x, y) {
        if (x == y) {
            return 0;
        } else if (x > y) {
            return 1;
        } else {
            return -1;
        }
	};
}();

console.log(f(3, 4));