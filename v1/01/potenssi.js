
function potenssi(x, y) {
    if (y == 1) {
        return x;
    }
    return y == 0 ? 1 : x * potenssi(x, --y);
}
console.log(potenssi(8, 4));