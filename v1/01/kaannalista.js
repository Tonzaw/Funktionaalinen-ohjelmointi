function kaannaLista(lista) {
    if (!lista.length) {
        return lista;
    }
    return kaannaLista(lista.slice(1)).concat(lista[0]);
}

var lista = [0,1,2,3,4,5,6,7,8,9];
console.log(kaannaLista(lista));